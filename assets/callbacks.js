if (!window.dash_clientside) {
    window.dash_clientside = {}
}

function consumptions_data(consumptions_smart_meters, consumptions_concentrators, consumptions_cellular_network, consumptions_core_network, consumptions_servers) {
    const dicts = [consumptions_smart_meters, consumptions_concentrators, consumptions_cellular_network, consumptions_core_network, consumptions_servers]
    var data = {'category': [], 'subcategory': [], 'consumption (J)': []}

    for (var i = 0; i < dicts.length; i++) {
        for (const [cat, subcats] of Object.entries(dicts[i])) {
            for (const [subcat, value] of Object.entries(subcats)) {
                data['category'].push(cat)
                data['subcategory'].push(subcat)
                data['consumption (J)'].push(value)
            }
        }
    }
    return data
}

function sum_consumption_dict(dict){
    var sum = 0
    for (const [cat, subcats] of Object.entries(dict))
        for (const [subcat, value] of Object.entries(subcats))
            sum += value
    return sum
}



window.dash_clientside.clientside = {
    update_sm: function(p_sm_idle_w, p_sm_active_w, r_g1, g1_factor,
        r_small_cluster, t_sm_small_cluster_active_s, t_sm_large_cluster_active_s,
        nb_sm, consumptions) {
            if (!r_g1 || !g1_factor || !r_small_cluster || !nb_sm)
                return window.dash_clientside.no_update
            
            const t_day_s = 86400

            consumptions['Smart meters']['small cluster G1 idle'] = Math.round(p_sm_idle_w * Math.min(t_day_s, t_sm_small_cluster_active_s * g1_factor) * nb_sm * r_small_cluster * r_g1)
            consumptions['Smart meters']['small cluster G1 active'] = Math.round(p_sm_active_w * Math.min(t_day_s, t_sm_small_cluster_active_s * g1_factor) * nb_sm * r_small_cluster * r_g1)
            consumptions['Smart meters']['small cluster G3 idle'] = Math.round(p_sm_idle_w * Math.min(t_day_s, t_sm_small_cluster_active_s * 1) * nb_sm * r_small_cluster * (1 - r_g1))
            consumptions['Smart meters']['small cluster G3 active'] = Math.round(p_sm_active_w * Math.min(t_day_s, t_sm_small_cluster_active_s * 1) * nb_sm * r_small_cluster * (1 - r_g1))
            consumptions['Smart meters']['large cluster G1 idle'] = Math.round(p_sm_idle_w * Math.min(t_day_s, t_sm_large_cluster_active_s * g1_factor) * nb_sm * (1 - r_small_cluster) * r_g1)
            consumptions['Smart meters']['large cluster G1 active'] = Math.round(p_sm_active_w * Math.min(t_day_s, t_sm_large_cluster_active_s * g1_factor) * nb_sm * (1 - r_small_cluster) * r_g1)
            consumptions['Smart meters']['large cluster G3 idle'] = Math.round(p_sm_idle_w * Math.min(t_day_s, t_sm_large_cluster_active_s * 1) * nb_sm * (1 - r_small_cluster) * (1 - r_g1))
            consumptions['Smart meters']['large cluster G3 active'] = Math.round(p_sm_active_w * Math.min(t_day_s, t_sm_large_cluster_active_s * 1) * nb_sm * (1 - r_small_cluster) * (1 - r_g1))
        return consumptions
    },

    update_ct: function(p_ct_static_w, r_gprs, p_gprs_w, p_wcdma_w, dr_gprs_kbps,
        dr_wcdma_kbps, dv_ct_bytes, nb_ct, consumptions) {
            if (!p_ct_static_w || !p_gprs_w || !p_wcdma_w || !dr_gprs_kbps || !dr_wcdma_kbps || !dv_ct_bytes || !nb_ct)
                return window.dash_clientside.no_update

            const t_day_s = 86400

            consumptions['Concentrators']['Static'] = Math.round(t_day_s * p_ct_static_w * nb_ct)
            consumptions['Concentrators']['GPRS'] = Math.round(nb_ct * r_gprs * p_gprs_w * dv_ct_bytes / (dr_gprs_kbps * 1e3 / 8))
            consumptions['Concentrators']['3G'] = Math.round(nb_ct * (1 - r_gprs) * p_wcdma_w * dv_ct_bytes / (dr_wcdma_kbps * 1e3 / 8))
            return consumptions
    },

    update_cellular: function(p_bts_w, dv_ct_bytes, nb_ct, r_gprs, bts_timeslots, dr_per_timeslot_kbps, bts_load,
        p_nb_w, dr_nb_kbps, nb_load, consumptions) {
            if (!p_bts_w || !dv_ct_bytes || !nb_ct || !bts_timeslots || !p_nb_w || !dr_nb_kbps)
                return window.dash_clientside.no_update
            
            consumptions['Cellular network']['GPRS'] = Math.round(p_bts_w * dv_ct_bytes * nb_ct * r_gprs / (bts_timeslots * (dr_per_timeslot_kbps * 1e3 / 8) * bts_load))
            consumptions['Cellular network']['3G'] = Math.round(p_nb_w * dv_ct_bytes * nb_ct * (1 - r_gprs) / ((dr_nb_kbps * 1e3 / 8) * nb_load))
            return consumptions
    },

    update_core: function(dv_ct_bytes, nb_ct, nb_edge_switch, nb_core_router,
        p_static_edge_switch_w, p_static_core_router_w, core_datarate_Gbps, core_max_datarate_Gbps,
        e_device_byte_j, e_device_pkt_j, core_load, pkt_size_bytes, consumptions) {
            if (!dv_ct_bytes || !nb_ct || !nb_edge_switch || !nb_core_router || !p_static_edge_switch_w || !p_static_core_router_w || !core_datarate_Gbps || !core_max_datarate_Gbps || !e_device_byte_j || !e_device_pkt_j || !core_load || !pkt_size_bytes)
                return window.dash_clientside.no_update

            const t_core_s = dv_ct_bytes * nb_ct / (core_datarate_Gbps * 1e9 / 8)
            consumptions['Core network']['Static'] = Math.round((nb_edge_switch * p_static_edge_switch_w + nb_core_router * p_static_core_router_w) * core_datarate_Gbps / (core_max_datarate_Gbps * core_load) * t_core_s)
            consumptions['Core network']['Dynamic'] = Math.round((nb_edge_switch + nb_core_router) * e_device_byte_j * dv_ct_bytes * nb_ct + (nb_edge_switch + nb_core_router) * e_device_pkt_j * dv_ct_bytes * nb_ct / pkt_size_bytes)
            return consumptions
    },

    update_servers: function(p_server_w, pue, monthly_visits, requests_per_visit,
        server_request_treatement_per_s, hdd_writing_MBps,
        request_peak_multiplier, nb_replicas, core_datarate_Gbps, consumptions) {
            if (!p_server_w || !pue || !monthly_visits || !requests_per_visit || !server_request_treatement_per_s || !hdd_writing_MBps || !request_peak_multiplier || !nb_replicas || !core_datarate_Gbps)
                return window.dash_clientside.no_update

            const t_day_s = 86400

            const requests_per_s = monthly_visits * requests_per_visit / (30 * 24 * 60 * 60)
            const nb_servers_requests = requests_per_s / server_request_treatement_per_s
            const nb_servers_writing = (core_datarate_Gbps / 8) * 1e3 / hdd_writing_MBps
            const nb_servers = Math.max(nb_servers_writing, nb_servers_requests * request_peak_multiplier)
            consumptions['Servers']['Static'] = Math.round(nb_servers * nb_replicas * t_day_s * p_server_w * pue)
            return consumptions
    },

    update_table: function(consumptions_smart_meters, consumptions_concentrators, consumptions_cellular_network, consumptions_core_network, consumptions_servers) {
            var data = consumptions_data(
                    consumptions_smart_meters,
                    consumptions_concentrators,
                    consumptions_cellular_network,
                    consumptions_core_network,
                    consumptions_servers
                )
            var sum = data['consumption (J)'].reduce((a, b) => a + b, 0)
            data['consumption (MWh)'] = []
            data['%'] = []
            for (var i = 0; i < data['consumption (J)'].length; i++) {
                data['consumption (MWh)'].push(Math.round(data['consumption (J)'][i] / (3600 * 1e6) * 100) / 100) // round(a * 100) / 100 -> to round to 2 decimal
                data['%'].push(Math.round(data['consumption (J)'][i] / sum * 100 * 100) / 100)
            }

            data['category'].push('Total')
            data['subcategory'].push('N/A')
            data['consumption (J)'].push(sum)
            data['consumption (MWh)'].push(Math.round(sum / (3600 * 1e6) * 100) / 100)
            data['%'].push(100)
            var table = []
            for (var i = 0; i < data['category'].length; i++) {
                var dic = {}
                dic['category'] = data['category'][i]
                dic['subcategory'] = data['subcategory'][i]
                dic['consumption (J)'] = data['consumption (J)'][i]
                dic['consumption (MWh)'] = data['consumption (MWh)'][i]
                dic['%'] = data['%'][i]
                table.push(dic)
            }

            var columns = []
            for (const [key, value] of Object.entries(data))
                columns.push({'name': key, 'id': key})
            
            return [table, columns, `Result: ${Math.round(sum / (3600 * 1e6) * 100) / 100}MWh per day`]
    },

    update_pie: function(consumptions_smart_meters, consumptions_concentrators, consumptions_cellular_network, consumptions_core_network, consumptions_servers) {
            var data = consumptions_data(
                    consumptions_smart_meters,
                    consumptions_concentrators,
                    consumptions_cellular_network,
                    consumptions_core_network,
                    consumptions_servers
                )

            var labels = [...new Set(data['category'])]
            var parents = ['','','','','']
            var values = [
                sum_consumption_dict(consumptions_smart_meters),
                sum_consumption_dict(consumptions_concentrators),
                sum_consumption_dict(consumptions_cellular_network),
                sum_consumption_dict(consumptions_core_network),
                sum_consumption_dict(consumptions_servers),
                ]
            
            var sum = values.reduce((a,b) => a + b, 0)

            for (var i = 0; i < values.length; i++)
                values[i] = Math.round(values[i] / sum * 10000) / 100

            var data_percent = []
            for (var i = 0; i < data['consumption (J)'].length; i++)
                data_percent.push(Math.round(data['consumption (J)'][i] / sum * 10000) / 100)

            console.log(data_percent)

            var sum_children = data_percent[0] + data_percent[1] + data_percent[2] + data_percent[3] + data_percent[4] + data_percent[5] + data_percent[6] + data_percent[7]
            values[0] = sum_children

            sum_children = data_percent[8] + data_percent[9] + data_percent[10]
            values[1] = sum_children

            sum_children = data_percent[11] + data_percent[12]
            values[2] = sum_children

            sum_children = data_percent[13] + data_percent[14]
            values[3] = sum_children

            sum_children = data_percent[15]
            values[4] = sum_children

            console.log(data_percent)

            var data_fig = [{
                type: 'sunburst',
                labels: labels.concat(data['subcategory']),
                parents: parents.concat(data['category']),
                values:  values.concat(data_percent),
                branchvalues: 'total',
                textinfo:'label+percent entry'
              }]
              
            var layout = {
                margin: {l: 0, r: 0, b: 0, t: 0},
              }
              
            var fig = {}
            fig['data'] = data_fig
            fig['layout'] = layout
            return fig
    },

    update_barplot: function(btn_clear, btn_remove, btn_add, input_case_name, input_case_name_placeholder, cases, consumptions_smart_meters, consumptions_concentrators, consumptions_cellular_network, consumptions_core_network, consumptions_servers) {
            const triggered = dash_clientside.callback_context.triggered
            if (triggered.length < 1)
                return window.dash_clientside.no_update

            const callback_id = triggered[0]['prop_id'].split('.')[0]

            if (!input_case_name || input_case_name == '') {
                input_case_name = 'A'
                while (cases['Case'].includes(input_case_name)) {
                    input_case_name = String.fromCharCode(input_case_name.charCodeAt(0) + 1)
                }
                input_case_name_placeholder = `Case name (default=${input_case_name})`
                if (callback_id == 'btn_add')
                    input_case_name_placeholder = `Case name (default=${String.fromCharCode(input_case_name.charCodeAt(0) + 1)})`
            }
            taken = cases['Case'].includes(input_case_name)
            if (callback_id == 'btn_clear') {
                input_case_name_placeholder = 'Case name (default=A)'
                cases = {'Case': [], 'Category': [], 'Consumption (MWh)': []}
            }
            else if (callback_id == 'btn_remove' && taken) {
                var i = 0
                while (cases['Case'][i] != input_case_name)
                    i++
                cases['Case'].splice(i, 5)
                cases['Category'].splice(i, 5)
                cases['Consumption (MWh)'].splice(i, 5)
            }
            else if (callback_id == 'btn_add' && !taken) {
                var data = consumptions_data(
                        consumptions_smart_meters,
                        consumptions_concentrators,
                        consumptions_cellular_network,
                        consumptions_core_network,
                        consumptions_servers
                    )
                var case_ = [input_case_name, input_case_name, input_case_name, input_case_name, input_case_name]
                var categories = ['Smart meters', 'Concentrators', 'Cellular network', 'Core network', 'Servers']
                var consumptions = [0,0,0,0,0]
                for (var i = 0; i < categories.length; i++)
                    for (var j = 0; j < data['category'].length; j++)
                        if (data['category'][j] == categories[i])
                            consumptions[i] += Math.round(data['consumption (J)'][j] / 3600 * 1e-6 * 100) / 100

                cases['Case'] = cases['Case'].concat(case_)
                cases['Category'] = cases['Category'].concat(categories)
                cases['Consumption (MWh)'] = cases['Consumption (MWh)'].concat(consumptions)
            }

            // Build the barplot
            var traces = [
                {name: 'Smart meters'},
                {name: 'Concentrators'},
                {name: 'Cellular network'},
                {name: 'Core network'},
                {name: 'Servers'},
            ]

            var x = []
            var y_list = [[], [], [], [], []]
            for (var i = 0; i < cases['Case'].length; i+=5) {
                x.push(cases['Case'][i])
                for (var j = 0; j < 5; j++) {
                    y_list[j].push(cases['Consumption (MWh)'][i+j])
                }
            }

            for (var i = 0; i < traces.length; i++) {
                traces[i]['type'] = 'bar'
                traces[i]['x'] = x
                traces[i]['y'] = y_list[i]
            }

            var layout = {
                barmode: 'stack'
              }
              
            var fig = {}
            fig['data'] = traces
            fig['layout'] = layout

            return [fig, input_case_name_placeholder, cases]
    },
};
