import dash
from dash import dcc
from dash import html
from dash import dash_table
from dash.dependencies import Input, Output, State, ClientsideFunction
from dash.exceptions import PreventUpdate
import pandas as pd
import plotly.express as px
import math
import numpy as np
# remove warning that should not happen from numpy when using "in"
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

df_parameters = pd.read_csv('parameters.csv')

app = dash.Dash(__name__)
server = app.server
app.title = 'Smart Metering Consumption'
app._favicon = ('icon.co')

app.layout = html.Div([
    html.Div([
        dcc.Store(id='dr_per_timeslot_kbps', data=df_parameters['dr_per_timeslot_kbps'][0]),
        dcc.Store(id='consumptions_smart_meters', data={'Smart meters': {}}),
        dcc.Store(id='consumptions_concentrators', data={'Concentrators': {}}),
        dcc.Store(id='consumptions_cellular_network', data={'Cellular network': {}}),
        dcc.Store(id='consumptions_core_network', data={'Core network': {}}),
        dcc.Store(id='consumptions_servers', data={'Servers': {}}),
        dcc.Store(id='cases', data= {'Case': [], 'Category': [], 'Consumption (MWh)': []}),
    ]),
    
    html.H1('End-to-End Energy Consumption of a Nation-Wide Smart Metering Infrastructure', style={'textAlign': 'center'}),
    dcc.Markdown('Several countries have deployed, or have started the deployment of a smart metering infrastructure in order to enable the **Smart Grid**. This infrastructure aims to provide new services to grid users and grid operators relying on several communication technologies. One of the goals of this infrastructure is **to improve energy consumption**, for instance by increasing the awareness of the users, or by enforcing energy management policies. Yet, **this infrastructure also consumes energy**. The objective of this work is to accurately **characterize the energy consumption of each part of the smart metering infrastructure**, at a nation-wide scale. This interface allows to explore several consumption scenarios highlighting the impact of legacy technologies on the energy consumption of the smart metering infrastructure.', style={'fontSize': 18}),
    dcc.Markdown('Full description of this work along with details on the hypotheses and models can be found [here](https://hal.archives-ouvertes.fr/hal-03666587).', style={'fontSize': 18}),
    dcc.Markdown('''To cite this work: “**Modeling the End-to-End Energy Consumption of a Nation-Wide Smart Metering Infrastructure**”, Adrien Gougeon, François Lemercier, Anne Blavette, Anne-Cécile Orgerie, *IEEE Symposium on Computers and Communications (ISCC)*, Rhodes Island, Greece, pages 1-7, June 2022.''', style={'fontSize': 18}),

    html.Div([
        dcc.Input(id='TEST_INPUT', placeholder='TEST INPUT', readOnly=True, type='text'),
    ]),

    dcc.Tabs([
        dcc.Tab(label='Smart meters', children=[
            # smart meters idle and active power
            html.Div([
                html.Div([
                    html.H4('Idle power consumption'),
                    dcc.Slider(
                        id='p_sm_idle_w',
                        min=min(df_parameters['p_sm_idle_w']),
                        max=max(df_parameters['p_sm_idle_w']),
                        step=0.1,
                        marks={round(i) if abs(i - round(i)) < 1e-3 else i:str(i) + 'W' for i in df_parameters['p_sm_idle_w'] if not math.isnan(i)},
                        value=0.2)
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Active power consumption'),
                    dcc.Slider(
                        id='p_sm_active_w',
                        min=min(df_parameters['p_sm_active_w']),
                        max=max(df_parameters['p_sm_active_w']),
                        step=1,
                        marks={round(i) if abs(i - round(i)) < 1e-3 else i:str(i) + 'W' for i in df_parameters['p_sm_active_w'] if not math.isnan(i)},
                        value=df_parameters['p_sm_active_w'][0])
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # ratio g1 / g3 and g1 efficiency factor
            html.Div([
                html.Div([
                    html.H4('G1 - G3 ratio'),
                    dcc.Slider(
                        id='r_g1',
                        min=0,
                        max=1,
                        step=0.1,
                        marks={0:'100% G3', 0.5:'50% G1 - 50% G3', 1:'100% G1'},
                        included=False,
                        value=df_parameters['r_g1'][0])
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('G1 factor (multiply the smart meters active time)'),
                    dcc.Input(
                        id='g1_factor',
                        value=df_parameters['g1_factor'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='float > 0')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # ratios of smart meters in small and large clusters and total number of smart meters
            html.Div([
                html.Div([
                    html.H4('Small cluster (rural) - Large cluster (urban) ratio '),
                    dcc.Slider(
                        id='r_small_cluster',
                        value=df_parameters['r_small_cluster'][0],
                        min=0,
                        max=1,
                        step=0.1,
                        marks={0:'100% large cluster', 0.5:'50% small - 50% large', 1:'100% small cluster'},
                        included=False,)
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Total number of smart meters'),
                    dcc.Input(
                        id='nb_sm',
                        value=df_parameters['nb_sm'][0],
                        type='number',
                        required=True,
                        placeholder='default = 35000000')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # active time in small and large clusters
            html.Div([
                html.Div([
                    html.H4('Active time for smart meters in small clusters'),
                    dcc.Slider(
                        id='t_sm_small_cluster_active_s',
                        min=min(df_parameters['t_sm_small_cluster_active_s']),
                        max=max(df_parameters['t_sm_small_cluster_active_s']),
                        step=None,
                        marks={round(i) if abs(i - round(i)) < 1e-3 else i:str(int(i/60)) + '\'' for i in df_parameters['t_sm_small_cluster_active_s'] if not math.isnan(i)},
                        value=df_parameters['t_sm_small_cluster_active_s'][0])
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Active time for smart meters in large clusters'),
                    dcc.Slider(
                        id='t_sm_large_cluster_active_s',
                        min=min(df_parameters['t_sm_large_cluster_active_s']),
                        max=max(df_parameters['t_sm_large_cluster_active_s']),
                        step=None,
                        marks={round(i) if abs(i - round(i)) < 1e-3 else i:str(int(i/3600)) + 'h' for i in df_parameters['t_sm_large_cluster_active_s'] if not math.isnan(i)},
                        value=df_parameters['t_sm_large_cluster_active_s'][0])
                    ], style={'width': '48%', 'display': 'inline-block'}),
            ]),
        ]),
        dcc.Tab(label='Concentrators', children=[
            # concentrators static power consumption and ratio gprs
            html.Div([
                html.Div([
                    html.H4('Static power consumption (W)'),
                    dcc.Input(
                        id='p_ct_static_w',
                        value=df_parameters['p_ct_static_w'][0],
                        type='number',
                        min=1,
                        max=1000,
                        required=True,
                        placeholder='default=30')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Ratio of concentrators with GPRS technology'),
                    dcc.Slider(
                        id='r_gprs',
                        min=0,
                        max=1,
                        step=0.1,
                        marks={0:'100% 3G', 0.5:'50% GPRS - 50% 3G', 1:'100% GPRS'},
                        value=df_parameters['r_gprs'][0],
                        included=False)
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # power consumption gprs and 3G
            html.Div([
                html.Div([
                    html.H4('GPRS power consumption (W)'),
                    dcc.Input(
                        id='p_gprs_w',
                        value=df_parameters['p_gprs_w'][0],
                        type='number',
                        min=0.1,
                        max=1000,
                        required=True,
                        placeholder='default=1.4')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('3G power consumption (W)'),
                    dcc.Input(
                        id='p_wcdma_w',
                        value=df_parameters['p_wcdma_w'][0],
                        type='number',
                        min=0.1,
                        max=1000,
                        required=True,
                        placeholder='default=2.1')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # datarate gprs and 3G
            html.Div([
                html.Div([
                    html.H4('Datarate GPRS (kbps)'),
                    dcc.Input(
                        id='dr_gprs_kbps',
                        value=df_parameters['dr_gprs_kbps'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 24')
                ], style={'width': '48%', 'display': 'inline-block'}
                ),
                html.Div([
                    html.H4('Datarate 3G (kbps)'),
                    dcc.Input(
                        id='dr_wcdma_kbps',
                        value=df_parameters['dr_wcdma_kbps'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 350')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # data volume per concentrator and number of concentrator
            html.Div([
                html.Div([
                    html.H4('Data volume per concentrator (bytes)'),
                    dcc.Input(
                        id='dv_ct_bytes',
                        value=df_parameters['dv_ct_bytes'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 150000')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Total number of concentrators'),
                    dcc.Input(
                        id='nb_ct',
                        value=df_parameters['nb_ct'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 770000')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
        ]),
        dcc.Tab(label='Cellular network', children=[
            # static power consumption of BTS and Node B
            html.Div([
                html.Div([
                    html.H4('BTS static power consumption (W)'),
                    dcc.Input(
                        id='p_bts_w',
                        value=df_parameters['p_bts_w'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 1780')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Node B static power consumption (W)'),
                    dcc.Input(
                        id='p_nb_w',
                        value=df_parameters['p_nb_w'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 800')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # bts time slots and node b data rate
            html.Div([
                html.Div([
                    html.H4('Time slots per BTS'),
                    dcc.Input(
                        id='bts_timeslots',
                        value=df_parameters['bts_timeslots'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 24')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Datarate Node B (kbps)'),
                    dcc.Input(
                        id='dr_nb_kbps',
                        value=df_parameters['dr_nb_kbps'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 100')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # bts load and node b load
            html.Div([
                html.Div([
                    html.H4('BTS average load'),
                    dcc.Slider(
                        id='bts_load',
                        min=0.1,
                        max=1,
                        step=0.05,
                        marks={round(i/10) if i==0 or i==10 else i/10:f'{i*10}%' for i in range(1,11)},
                        included=False,
                        value=df_parameters['bts_load'][0])
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Node B average load'),
                    dcc.Slider(
                        id='nb_load',
                        min=0.1,
                        max=1,
                        step=0.05,
                        marks={round(i/10) if i==0 or i==10 else i/10:f'{i*10}%' for i in range(1,11)},
                        included=False,
                        value=df_parameters['nb_load'][0])
                ], style={'width': '48%', 'display': 'inline-block'}),
            ]),
        ]),
        dcc.Tab(label='Core network', children=[
            dcc.Markdown('''
            Depends on the data volume per smart meters and the number of smart meters.
            Note: core datarate used does NOT have an impact on the consumption (higher datarate increases consumption but reduces time of use)
            '''),
            # nb edge switches and nb core routers
            html.Div([
                html.Div([
                    html.H4('Number of edge switchs'),
                    dcc.Input(
                        id='nb_edge_switch',
                        value=df_parameters['nb_edge_switch'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 8')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Number of core routers'),
                    dcc.Input(
                        id='nb_core_router',
                        value=df_parameters['nb_core_router'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 1')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # static power consumption of edge switches and core routers
            html.Div([
                html.Div([
                    html.H4('Edge switch static power consumption (W)'),
                    dcc.Input(
                        id='p_static_edge_switch_w',
                        value=df_parameters['p_static_edge_switch_w'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 150')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Core router static power consumption (W)'),
                    dcc.Input(
                        id='p_static_core_router_w',
                        value=df_parameters['p_static_core_router_w'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 555')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # used datarate and maximum datarate
            html.Div([
                html.Div([
                    html.H4('Datarate used of network devices'),
                    dcc.Input(
                        id='core_datarate_Gbps',
                        value=df_parameters['core_datarate_Gbps'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 1')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Maximum datarate of network devices'),
                    dcc.Input(
                        id='core_max_datarate_Gbps',
                        value=df_parameters['core_max_datarate_Gbps'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 48')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # energy per bytes and per packet
            html.Div([
                html.Div([
                    html.H4('Energy per byte (J)'),
                    dcc.Input(
                        id='e_device_byte_j',
                        value=df_parameters['e_device_byte_j'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder=f'default = 3.4e-9')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Energy per packet (J)'),
                    dcc.Input(
                        id='e_device_pkt_j',
                        value=df_parameters['e_device_pkt_j'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 192e-9')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # average load and packet packet size
            html.Div([
                html.Div([
                    html.H4('Average load of network devices'),
                    dcc.Input(
                        id='core_load',
                        value=df_parameters['core_load'][0],
                        type='number',
                        min=0.01,
                        max=1,
                        required=True,
                        placeholder='default = 0.25')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Packet size (bytes)'),
                    dcc.Input(
                        id='pkt_size_bytes',
                        value=df_parameters['pkt_size_bytes'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 1450')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
        ]),
        dcc.Tab(label='Servers', children=[
            # Static power of a server and pue
            html.Div([
                html.Div([
                    html.H4('Static power consumption of a server (W)'),
                    dcc.Input(
                        id='p_server_w',
                        value=df_parameters['p_server_w'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 108')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Power Usage Effectiveness (PUE)'),
                    dcc.Input(
                        id='pue',
                        value=df_parameters['pue'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 1.7')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # monthly visits and request per visit
            html.Div([
                html.Div([
                    html.H4('Monthly visits of the website'),
                    dcc.Input(
                        id='monthly_visits',
                        value=df_parameters['monthly_visits'][0],
                        type='number',
                        min=0,
                        required=True,
                        placeholder='default = 540000')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Requests per visit'),
                    dcc.Input(
                        id='requests_per_visit',
                        value=df_parameters['requests_per_visit'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 2.38')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # request served by second by a server and disk writing speed
            html.Div([
                html.Div([
                    html.H4('Requests treated per second by a server'),
                    dcc.Input(
                        id='server_request_treatement_per_s',
                        value=df_parameters['server_request_treatement_per_s'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 200')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Server disk writing speed (MBps)'),
                    dcc.Input(
                        id='hdd_writing_MBps',
                        value=df_parameters['hdd_writing_MBps'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 150')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
            # request peak multiplier and number of replicas of the servers
            html.Div([
                html.Div([
                    html.H4('Request peak multiplier'),
                    dcc.Input(
                        id='request_peak_multiplier',
                        value=df_parameters['request_peak_multiplier'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 10')
                ], style={'width': '48%', 'display': 'inline-block'}),

                html.Div([
                    html.H4('Number of replicas of the servers'),
                    dcc.Input(
                        id='nb_replicas',
                        value=df_parameters['nb_replicas'][0],
                        type='number',
                        min=1,
                        required=True,
                        placeholder='default = 3')
                ], style={'width': '48%', 'float': 'right', 'display': 'inline-block'}),
            ]),
        ])
    ]),

    dcc.Markdown('''---'''),
    html.Div([
        html.H2('Result', id='title_result', style={'width': '48%', 'display': 'inline-block'}),
        html.Button('Clear all cases', id='btn_clear', style={'width': '10%', 'float': 'right', 'display': 'inline-block'}),
        html.Button('Remove case', id='btn_remove', style={'width': '10%', 'float': 'right', 'display': 'inline-block'}),
        html.Button('Add case for comparison', id='btn_add', style={'width': '10%', 'float': 'right', 'display': 'inline-block'}),
        dcc.Input(id='input_case_name', type='text', placeholder='Case name (default=A)', style={'width': '10%', 'float': 'right', 'display': 'inline-block'}),
    ]),

    dcc.Tabs([
        dcc.Tab(label='One case', children=[
            html.Div([
                html.Div([
                    dash_table.DataTable(
                        id='table',
                        sort_action='native', 
                        style_data={
                            'color': 'black',
                            'backgroundColor': 'white'
                            },
                        style_data_conditional=[{
                            'if': {'row_index': 'odd'},
                            'backgroundColor': 'rgb(220, 220, 220)',
                            }],
                        style_header={
                            'backgroundColor': 'rgb(210, 210, 210)',
                            'color': 'black',
                            'fontWeight': 'bold'
                        }
                    )
                ], style={'width': '48%', 'display': 'inline-block'}),
                dcc.Graph(id='pie-chart', style={'width': '48%', 'height': '550px', 'float': 'right', 'display': 'inline-block'}),
            ]),
        ]),
        dcc.Tab(label='Comparison', children=[
            dcc.Graph(id='barplot', style={'width': '100%', 'height': '550px', 'float': 'right', 'display': 'inline-block'}),
        ]),
    ]),
])

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_sm'
    ),
    # Output('update_sm_callback', 'value'),
    Output('consumptions_smart_meters', 'data'),
    Input('p_sm_idle_w', 'value'),
    Input('p_sm_active_w', 'value'),
    Input('r_g1', 'value'),
    Input('g1_factor', 'value'),
    Input('r_small_cluster', 'value'),
    Input('t_sm_small_cluster_active_s', 'value'),
    Input('t_sm_large_cluster_active_s', 'value'),
    Input('nb_sm', 'value'),
    State('consumptions_smart_meters', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_ct'
    ),
    Output('consumptions_concentrators', 'data'),
    Input('p_ct_static_w', 'value'),
    Input('r_gprs', 'value'),
    Input('p_gprs_w', 'value'),
    Input('p_wcdma_w', 'value'),
    Input('dr_gprs_kbps', 'value'),
    Input('dr_wcdma_kbps', 'value'),
    Input('dv_ct_bytes', 'value'),
    Input('nb_ct', 'value'),
    State('consumptions_concentrators', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_cellular'
    ),    
    Output('consumptions_cellular_network', 'data'),
    Input('p_bts_w', 'value'),
    Input('dv_ct_bytes', 'value'),
    Input('nb_ct', 'value'),
    Input('r_gprs', 'value'),
    Input('bts_timeslots', 'value'),
    Input('dr_per_timeslot_kbps', 'data'),
    Input('bts_load', 'value'),
    Input('p_nb_w', 'value'),
    Input('dr_nb_kbps', 'value'),
    Input('nb_load', 'value'),
    State('consumptions_cellular_network', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_core'
    ),   
    Output('consumptions_core_network', 'data'),
    Input('dv_ct_bytes', 'value'),
    Input('nb_ct', 'value'),
    Input('nb_edge_switch', 'value'),
    Input('nb_core_router', 'value'),
    Input('p_static_edge_switch_w', 'value'),
    Input('p_static_core_router_w', 'value'),
    Input('core_datarate_Gbps', 'value'),
    Input('core_max_datarate_Gbps', 'value'),
    Input('e_device_byte_j', 'value'),
    Input('e_device_pkt_j', 'value'),
    Input('core_load', 'value'),
    Input('pkt_size_bytes', 'value'),
    State('consumptions_core_network', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_servers'
    ),   
    Output('consumptions_servers', 'data'),
    Input('p_server_w', 'value'),
    Input('pue', 'value'),
    Input('monthly_visits', 'value'),
    Input('requests_per_visit', 'value'),
    Input('server_request_treatement_per_s', 'value'),
    Input('hdd_writing_MBps', 'value'),
    Input('request_peak_multiplier', 'value'),
    Input('nb_replicas', 'value'),
    Input('core_datarate_Gbps', 'value'),
    State('consumptions_servers', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_table'
    ),   
    Output('table', 'data'),
    Output('table', 'columns'),
    Output('title_result', 'children'),
    Input('consumptions_smart_meters', 'data'),
    Input('consumptions_concentrators', 'data'),
    Input('consumptions_cellular_network', 'data'),
    Input('consumptions_core_network', 'data'),
    Input('consumptions_servers', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_pie'
    ), 
    Output('pie-chart', 'figure'),
    Input('consumptions_smart_meters', 'data'),
    Input('consumptions_concentrators', 'data'),
    Input('consumptions_cellular_network', 'data'),
    Input('consumptions_core_network', 'data'),
    Input('consumptions_servers', 'data'),
)

app.clientside_callback(
    ClientsideFunction(
        namespace='clientside',
        function_name='update_barplot'
    ), 
    Output('barplot', 'figure'),
    Output('input_case_name', 'placeholder'),
    Output('cases', 'data'),
    Input('btn_clear', 'n_clicks'),
    Input('btn_remove', 'n_clicks'),
    Input('btn_add', 'n_clicks'),
    State('input_case_name', 'value'),
    State('input_case_name', 'placeholder'),
    State('cases', 'data'),
    State('consumptions_smart_meters', 'data'),
    State('consumptions_concentrators', 'data'),
    State('consumptions_cellular_network', 'data'),
    State('consumptions_core_network', 'data'),
    State('consumptions_servers', 'data'),
)

if __name__ == '__main__':
    app.run_server(debug=False)