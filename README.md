This project contains the code to build and deploy a website displaying the consumption of the Advanced Metering Infrastructure based on several customizable parameters. 

This work is based on the paper [**Modeling the End-to-End Energy Consumption of a Nation-Wide Smart Metering Infrastructure**](https://hal.archives-ouvertes.fr/hal-03666587).


Gitlab Pages Deployment
-------------------------

- To build the website locally use the following command: 

```
./build.sh
```
The folder `public` contains the website.


- You can also run the python application locally using the following command:

```
python app.py
```

---


Heroku Deployment
-------------------------

> Warning: heroku free plan is over. The website is now fully clientside and deployed using gitlab pages.

For a first deployment use the following commands:


```
cd heroku_version
echo web: gunicorn app:server > Procfile
pip freeze > requirements.txt
heroku create smart-grid-network
git init
git add .
git commit -m 'deployment'
git push heroku master
heroku ps:scale web=1

```

Otherwise simply update the heroku git

For further information see https://dash.plotly.com/deployment
or visit heroku help


