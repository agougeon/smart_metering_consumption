#!/bin/bash

# handle the case the web site isn't deployed at the root of the website
# For instance on Gitlab pages
BASE_DIR=$1

set -xe

# Launch app and wait until properly launched
python3 app.py &
sleep 3

# Retrieve necessary components of the app
wget -r http://127.0.0.1:8050/ 
wget -r http://127.0.0.1:8050/_dash-layout 
wget -r http://127.0.0.1:8050/_dash-dependencies
wget http://127.0.0.1:8050/_dash-component-suites/dash/dash_table/async-table.js
wget http://127.0.0.1:8050/_dash-component-suites/dash/dash_table/async-highlight.js
mv *.js 127.0.0.1:8050/_dash-component-suites/dash/dash_table/
wget http://127.0.0.1:8050/_dash-component-suites/dash/dcc/async-graph.js
wget http://127.0.0.1:8050/_dash-component-suites/dash/dcc/async-plotlyjs.js
wget http://127.0.0.1:8050/_dash-component-suites/dash/dcc/async-markdown.js
wget http://127.0.0.1:8050/_dash-component-suites/dash/dcc/async-highlight.js
wget http://127.0.0.1:8050/_dash-component-suites/dash/dcc/async-slider.js
mv *.js 127.0.0.1:8050/_dash-component-suites/dash/dcc/

# Add our head with plotly library for javascript
sed -i '/<head>/ r head.html' 127.0.0.1:8050/index.html

# Rename some files and adjust other files accordingly
sed -i "s/_dash-layout/${BASE_DIR}\/_dash-layout.json/g" 127.0.0.1:8050/_dash-component-suites/dash/dash-renderer/build/*.js
sed -i "s/_dash-dependencies/${BASE_DIR}\/_dash-dependencies.json/g" 127.0.0.1:8050/_dash-component-suites/dash/dash-renderer/build/*.js

mv 127.0.0.1:8050/_dash-layout 127.0.0.1:8050/_dash-layout.json	
mv 127.0.0.1:8050/_dash-dependencies 127.0.0.1:8050/_dash-dependencies.json


# intent: fix pathes to script
sed -i "s/\/_dash-component-suites/\/${BASE_DIR}\/_dash-component-suites/g" 127.0.0.1:8050/index.html
sed -i "s/\/assets/\/${BASE_DIR}\/assets/g" 127.0.0.1:8050/index.html

# Copy callbacks and icon
cp assets/* 127.0.0.1:8050/assets/

# Rename website folder
rm -rf public
mv 127.0.0.1:8050 public

# Kill app
ps -ef | grep app.py | awk 'NR==1{print $2}' | xargs kill
