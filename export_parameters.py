import pandas as pd

d = {
'p_sm_idle_w': [0.1, 0.5, 1],
'p_sm_active_w': [1, 5, 15],
'r_g1': [0.2],
'g1_factor': [14],
'r_small_cluster': [0.2],
't_sm_small_cluster_active_s': [600, 1200, 1800],
't_sm_large_cluster_active_s': [3600, 21600, 43200],
'nb_sm': [35000000],
'p_ct_static_w': [15],
'r_gprs': [0.2],
'p_gprs_w': [1.4],
'p_wcdma_w': [2.1],
'dr_gprs_kbps': [24],
'dr_wcdma_kbps': [350],
'dv_ct_bytes': [150000],
'nb_ct': [770000],
'p_bts_w': [1430],
'bts_timeslots': [3 * 8],
'dr_per_timeslot_kbps': [12],
'bts_load': [0.3],
'p_nb_w': [1450],
'dr_nb_kbps': [1361],
'nb_load': [0.3],
'core_datarate_Gbps': [1],
'nb_edge_switch': [8],
'nb_core_router': [1],
'p_static_edge_switch_w': [150],
'p_static_core_router_w': [555],
'e_device_byte_j': [3.4 * 1e-9],
'e_device_pkt_j': [192 * 1e-9],
'pkt_size_bytes': [1450],
'core_load': [0.25],
'core_max_datarate_Gbps': [48],
'monthly_visits': [540000],
'requests_per_visit': [2.38],
'server_request_treatement_per_s': [200],
'hdd_writing_MBps': [150],
'request_peak_multiplier': [10],
'nb_replicas': [3],
'p_server_w': [108],
'pue': [1.7],
}

df = pd.DataFrame({key:pd.Series(value) for key,value in d.items()})
df.to_csv('parameters.csv', index=False)
